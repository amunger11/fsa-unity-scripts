using System;
using System.Xml;
using System.Xml.Linq;
using Hyland.Unity;


namespace UnityScripts
{
    /// <summary>
    /// gets keywords and e-form fields, parses them, and creates an XML file
    /// for Infinium.
    /// </summary>
    public class trGetTradeDataToXML : IWorkflowScript
    {
        #region User-Configurable Script Settings
        // Script name for diagnostics logging
        private const string ScriptName = "Trade Output Script (No Exceptions)";

        // Diagnostics logging level - set to Verbose for testing, Error for production
        private const Diagnostics.DiagnosticsLevel DiagLevel = Diagnostics.DiagnosticsLevel.Verbose;

        // Workflow property where error message will be stored
        private const string ErrorMessageProperty = "UnityError";

        // If true, errors will be added to document history
        private const bool WriteErrorsToHistory = true;

        // Date/Time format for diagnostics logging
        private const string DateTimeFormat = "MM-dd-yyyy HH:mm:ss.fff";


        // Export File Path
//        private const string DESTPATH = @"\\Docimages\apdev$\OB Processing Files\XML Export Files\Trade Invoice Exports";
        private const string DESTPATH = @"c:\Output\FSA";
        // Keyword Type Names
        private const string KW_INVOICENUM = "Invoice Number";
        private const string KW_PONUM = "PO Number";
        private const string KW_INVOICEDATE = "Invoice Date";
        private const string KW_INVOICEDUEDATE = "Invoice Due Date";
        private const string KW_DISCOUNTDUEDATE = "Discount Due Date";
        private const string KW_RECEIVERNUM = "Receiver Number";
        private const string KW_VENDORNUM = "Purchasing Vendor Number";
        private const string KW_COMPANY = "Company";
        private const string KW_DIVISION = "Division";
        private const string KW_LOCATION = "Location";
        private const string KW_WAREHOUSE = "Warehouse";
        private const string KW_REMITADDRCODE = "Remit to Address Code";
        private const string KW_TAX = "Tax";
        private const string KW_FREIGHT = "Freight";
        private const string KW_DROPSHIP = "Drop Ship";
        private const string KW_ADJINVAMT = "Adjusted Invoice Amount";
        private const string KW_POAMT = "PO Amount";
        private const string KW_DISCOUNT = "Discount Amount";

        // XML Field Names
        private const string XML_ROOT = "Trade_Invoice";
        private const string XML_INVOICENUM = "Invoice_Number";
        private const string XML_PONUM = "PO_Number";
        private const string XML_INVOICEDATE = "Invoice_Date";
        private const string XML_INVOICEDUEDATE = "Invoice_Due_Date";
        private const string XML_DISCOUNTDUEDATE = "Discount_Due_Date";
        private const string XML_RECEIVERNUM = "Receiver_Number";
        private const string XML_VENDORNUM = "Purchasing_Vendor_Number";
        private const string XML_COMPANY = "Company";
        private const string XML_DIVISION = "Division";
        private const string XML_LOCATION = "Location";
        private const string XML_WAREHOUSE = "Warehouse";
        private const string XML_REMITADDRCODE = "Remit_to_Address_Code";
        private const string XML_TAX = "Tax";
        private const string XML_FREIGHT = "Freight";
        private const string XML_DROPSHIP = "Drop_Ship";
        private const string XML_ADJINVAMT = "Invoice_Amount";
        private const string XML_POAMT = "PO_Amount";
        private const string XML_DISCOUNT = "Discount_Amount";

        #endregion

        /***********************************************
         * USER/SE: PLEASE DO NOT EDIT BELOW THIS LINE *
         ***********************************************/

        #region Private Globals
        // Active workflow document
        private Document _currentDocument;
      //  private const long TestDocId = 15154;
        private const long TestDocId = 12823;  // Invoice
        #endregion

        #region IWorkflowScript
        /// <summary>
        /// Implementation of <see cref="IWorkflowScript.OnWorkflowScriptExecute" />.
        /// <seealso cref="IWorkflowScript" />
        /// </summary>
        /// <param name="app">Unity Application object</param>
        /// <param name="args">Workflow event arguments</param>
        public void OnWorkflowScriptExecute(Application app, WorkflowEventArgs args)
       // public void OnWorkflowScriptExecute(Application app, WorkflowEventArgs args=null)
        {
            try
            {
                // Initialize global settings
                InitializeScript(ref app, ref args);
	
	            // Currently active document returned by the root automation object
	            // The script will only have one CurrentDocument during each execution	
                Document doc = _currentDocument;
        	
	            // Get doc handle, for naming the file
	            string strDocID = doc.ID.ToString();

                //	'Keyword collection for the current document
            	// Declare variables for xml output, assign "" by default.

                string sInvoiceNum = "";
                string sPONum = "";
                string sInvoiceDate = "";
                string sInvoiceDueDate = "";
	            string sDiscountDueDate = "";
	            string sReceiverNum = "";
	            string sVendorNum = "";
	            string sCompany = "";
	            string sDivision = "";
	            string sLocation = "";
	            string sWarehouse = "";
	            string sRemitAddrCode = "";
	            string sTax = "";
	            string sFreight = "";
	            string sDropShip = "";
	            string sAdjInvAmt = "";
	            string sPOAmt = "";
	            string sDiscount = "";
               
                string output = "";
                // Iterate through all Keyword Records
                foreach (KeywordRecord keywordRecord in doc.KeywordRecords)
                {
                    foreach (Keyword keyword in keywordRecord.Keywords)
                    {
                        if (keyword.IsBlank)
                        {
                            output += string.Format("  {0} is blank",
                            keyword.KeywordType.Name);
                        }
                        else
                        {
                            output += string.Format("  {0} = {1}",
                            keyword.KeywordType.Name, keyword.Value.ToString());

                            switch (keyword.KeywordType.Name)
                            {
                                case KW_INVOICENUM:
                                    sInvoiceNum = keyword.Value.ToString();
                                    break;
                                case KW_PONUM:
                                    sPONum = keyword.Value.ToString();
                                    break;
                                case KW_INVOICEDATE:
                                    sInvoiceDate = keyword.Value.ToString();
                                    break;
                                case KW_INVOICEDUEDATE:
                                    sInvoiceDueDate = keyword.Value.ToString();
                                    break;
                                case KW_DISCOUNTDUEDATE: 
                                    sDiscountDueDate = keyword.Value.ToString();
                                    break;
                                case KW_RECEIVERNUM:
                                    sReceiverNum = keyword.Value.ToString();
                                    break;
                                case KW_VENDORNUM:
                                    sVendorNum = keyword.Value.ToString();
                                    break;
                                case KW_COMPANY:
                                    sCompany = keyword.Value.ToString();
                                    break;
                                case KW_DIVISION:
                                    sDivision = keyword.Value.ToString();
                                    break;
                                case KW_LOCATION:
                                    sLocation = keyword.Value.ToString();
                                    break;
                                case KW_WAREHOUSE:
                                    sWarehouse = keyword.Value.ToString();
                                    break;
                                case KW_REMITADDRCODE:
                                    sRemitAddrCode = keyword.Value.ToString();
                                    break;
                                case KW_TAX:
                                    sTax = keyword.Value.ToString();
                                    break;
                                case KW_FREIGHT:
                                    sFreight = keyword.Value.ToString();
                                    break;
                                case KW_DROPSHIP:
                                    sDropShip = keyword.Value.ToString();
                                    break;
                                case KW_ADJINVAMT:
                                    sAdjInvAmt = keyword.Value.ToString();
                                    break;
                                case KW_POAMT:
                                    sPOAmt = keyword.Value.ToString();
                                    break;
                                case KW_DISCOUNT:
                                    sDiscount = keyword.Value.ToString();
                                    break;
                            }
	                    }
                    }
                }
                if (sTax != ""){sTax.Replace("$","");sTax.Replace(",","");}
                if (sFreight != "") { sFreight.Replace("$",""); sFreight.Replace(",",""); }
                if (sAdjInvAmt != "") { sAdjInvAmt.Replace("$",""); sAdjInvAmt.Replace(",",""); }
                if (sPOAmt != "") { sPOAmt.Replace("$",""); sPOAmt.Replace(",",""); }
                if (sDiscount != "") { sDiscount.Replace("$",""); sDiscount.Replace(",",""); }

                app.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, output);

                XDocument xmlDoc = new XDocument(
                    new XElement(XML_ROOT,
                    new XElement(XML_INVOICENUM, sInvoiceNum),
                    new XElement(XML_PONUM, sPONum),
                    new XElement(XML_INVOICEDATE, sInvoiceDate),
                    new XElement(XML_INVOICEDUEDATE, sInvoiceDueDate),
                    new XElement(XML_DISCOUNTDUEDATE, sDiscountDueDate),
                    new XElement(XML_RECEIVERNUM, sReceiverNum),
                    new XElement(XML_VENDORNUM, sVendorNum),
                    new XElement(XML_COMPANY, sCompany),
                    new XElement(XML_DIVISION, sDivision),
                    new XElement(XML_LOCATION, sLocation),
                    new XElement(XML_WAREHOUSE, sWarehouse),
                    new XElement(XML_REMITADDRCODE, sDiscountDueDate),
                    new XElement(XML_TAX, sTax),
                    new XElement(XML_FREIGHT, sFreight),
                    new XElement(XML_DROPSHIP, sDropShip),
                    new XElement(XML_ADJINVAMT, sAdjInvAmt),
                    new XElement(XML_POAMT, sPOAmt),
                    new XElement(XML_DISCOUNT, sDiscount)));
                
                // Create folder if it doesn't exist
                bool exists = System.IO.Directory.Exists(DESTPATH);

                if (!exists)
                    System.IO.Directory.CreateDirectory(DESTPATH);
                
                bool newExists = System.IO.Directory.Exists(DESTPATH);
                if (!newExists) throw new ApplicationException(string.Format("Could not create output folder: {0}.",DESTPATH)); 

                string sFilePath = string.Format(@"{0}\{1}.xml",DESTPATH,strDocID);
                XmlWriterSettings xws = new XmlWriterSettings { OmitXmlDeclaration = true };
                using (XmlWriter xw = XmlWriter.Create(sFilePath, xws))
                xmlDoc.Save(xw);
            }
            catch (Exception ex)
            {
                // Handle exceptions and log to Diagnostics Console and document history
                HandleException(ex, ref app, ref args);
            }
            finally
            {
                // Log script execution end
                app.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Info,
                string.Format("End Script - [{0}]", ScriptName));
            }
        }
    #endregion

        #region Helper Functions
        /// <summary>
        /// Initialize global settings
        /// </summary>
        /// <param name="app">Unity Application object</param>
        /// <param name="args">Workflow event arguments</param>
        private void InitializeScript(ref Application app, ref WorkflowEventArgs args)
        {
            // Set the specified diagnostics level
            app.Diagnostics.Level = DiagLevel;

            // Log script execution start
            app.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Info,
                string.Format("{0} - Start Script - [{1}]", DateTime.Now.ToString(DateTimeFormat), ScriptName));

            // Capture active document as global
            //_currentDocument = app.Core.GetDocumentByID(TestDocId);
            _currentDocument = args.Document;
            // If an error was stored in the property bag from a previous execution, clear it
            if (args.SessionPropertyBag.ContainsKey(ErrorMessageProperty)) args.SessionPropertyBag.Remove(ErrorMessageProperty);

            // Set ScriptResult = true for workflow rules (will become false if an error is caught)
            args.ScriptResult = true;
        }

        /// <summary>
        /// Handle exceptions and log to Diagnostics Console and document history
        /// </summary>
        /// <param name="ex">Exception</param>
        /// <param name="app">Unity Application object</param>
        /// <param name="args">Workflow event arguments</param>
        /// <param name="otherDocument">Document on which to update history if not active workflow document</param>
        private void HandleException(Exception ex, ref Application app, ref WorkflowEventArgs args, Document otherDocument = null)
        {
            var history = app.Core.LogManagement;
            bool isInner = false;

            // Cycle through all inner exceptions
            while (ex != null)
            {
                // Construct error text to store to workflow property
                string propertyError = string.Format("{0}{1}: {2}",
                    isInner ? "Inner " : "",
                    ex.GetType().Name,
                    ex.Message);

                // Construct error text to store to document history
                string historyError = propertyError.Replace(ex.GetType().Name, "Unity Script Error");

                // Construct error text to log to diagnostics console
                string diagnosticsError = string.Format("{0} - ***ERROR***{1}{2}{1}{1}Stack Trace:{1}{3}",
                    DateTime.Now.ToString(DateTimeFormat),
                    Environment.NewLine,
                    propertyError,
                    ex.StackTrace);

                // Add error message to document history (on current or specified document)
                var document = otherDocument ?? _currentDocument;
                if (historyError.Length > 200) historyError = historyError.Substring(0, 199);
                if (document != null && WriteErrorsToHistory) history.CreateDocumentHistoryItem(document, historyError);

                // Write error message to Diagnostcs Consonle
                app.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, diagnosticsError);

                // Store the original (inner) exception message to error workflow property
                if (ex.InnerException == null) args.SessionPropertyBag.Set(ErrorMessageProperty, ex.Message);

                // Move on to next inner exception
                ex = ex.InnerException;
                isInner = true;
            }

            // Set ScriptResult = false for workflow rules
            args.ScriptResult = false;
        }
        private Keyword CreateKeywordHelper(KeywordType Keytype, string Value)
        {
            Keyword key = null;
            switch (Keytype.DataType)
            {
                case KeywordDataType.Currency:
                case KeywordDataType.Numeric20:
                    decimal decVal = decimal.Parse(Value);
                    key = Keytype.CreateKeyword(decVal);
                    break;
                case KeywordDataType.Date:
                case KeywordDataType.DateTime:
                    DateTime dateVal = DateTime.Parse(Value);
                    key = Keytype.CreateKeyword(dateVal);
                    break;
                case KeywordDataType.FloatingPoint:
                    double dblVal = double.Parse(Value);
                    key = Keytype.CreateKeyword(dblVal);
                    break;
                case KeywordDataType.Numeric9:
                    long lngVal = long.Parse(Value);
                    key = Keytype.CreateKeyword(lngVal);
                    break;
                default:
                    key = Keytype.CreateKeyword(Value);
                    break;
            }
            return key;
        }

        #endregion
    }
}