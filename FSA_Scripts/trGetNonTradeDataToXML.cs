﻿using System;
using System.Collections.Generic;
using Hyland.Unity;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml;

namespace FSA_Scripts
{
    public class trGetNonTradeDataToXML : Hyland.Unity.IWorkflowScript
    {
        #region User-Configurable Script Settings
        // Diagnostics logging level - set to Verbose for testing, Error for production
        private const Diagnostics.DiagnosticsLevel DiagLevel = Diagnostics.DiagnosticsLevel.Verbose;

        private const string ScriptName = "Get Non-Trade Data to XML script";

        // Workflow property where error message will be stored
        private const string ErrorMessageProperty = "UnityError";

        // If true, errors will be added to document history
        private const bool WriteErrorsToHistory = true;

        // Date/Time format for diagnostics logging
        private const string DateTimeFormat = "MM-dd-yyyy HH:mm:ss.fff";

        private const string DESTPATH = @"C:\test Files\FSANonTrade";

        // Keyword Type Names
        private const string KW_COMPANY = "Company";
        private const string KW_DIVISION = "Division";
        private const string KW_INVOICENUM = "Invoice Number";
        private const string KW_PONUM = "PO Number";
        private const string KW_INVOICEDATE = "Invoice Date";
        private const string KW_INVOICEDUEDATE = "Invoice Due Date";
        private const string KW_DISCOUNTDUEDATE = "Discount Due Date";
        private const string KW_DISCOUNT = "Discount Amount";
        private const string KW_VENDORNUM = "Payables Vendor Number";
        private const string KW_REMITADDRCODE = "Remit to Address Code";
        private const string KW_TAX = "Adjusted Tax";
        private const string KW_ADJINVAMT = "Adjusted Invoice Amount";
        private const string KW_BUSDATE = "Business Date";
        private const string KW_INVDESC = "Invoice Description";
        private const string KW_PAYMSG = "Payment Message";

        // E-Form Field Names
        private const string EF_XMLFIELD = "inpGL_ItemsXML";

        // E-Form Field Structure
        private const string EF_GLLINE = "GL_Line";
        private const string EF_GLCODE = "glCode";
        private const string EF_GLAMT = "amount";
        private const string EF_PROJNUM = "projectNum";
        private const string EF_ACTCODE = "activityCode";
        private const string EF_COSTCODE = "costCode";

        // XML Field Names
        private const string XML_ROOT = "Non-Trade_Invoice";
        private const string XML_COMPANY = "Company";
        private const string XML_DIVISION = "Division";
        private const string XML_INVOICENUM = "Invoice_Number";
        private const string XML_PONUM = "PO_Number";
        private const string XML_INVOICEDATE = "Invoice_Date";
        private const string XML_INVOICEDUEDATE = "Invoice_Due_Date";
        private const string XML_DISCOUNTDUEDATE = "Discount_Due_Date";
        private const string XML_DISCOUNT = "Discount_Amount";
        private const string XML_VENDORNUM = "Payables_Vendor_Number";
        private const string XML_REMITADDRCODE = "Remit_to_Address_Code";
        private const string XML_TAX = "Adjusted_Tax";
        private const string XML_ADJINVAMT = "Invoice_Amount";
        private const string XML_BUSDATE = "Business_Date";
        private const string XML_INVDESC = "Invoice_Description";
        private const string XML_PAYMSG = "Payment_Message";
        private const string XML_GLLINE = "GL_Line";
        private const string XML_GLCODE = "GL_Non-Trade_Account_Code";
        private const string XML_GLAMT = "GL_Non-Trade_Item_Amount";
        private const string XML_PROJNUM = "PA_Project_Number";
        private const string XML_ACTCODE = "PA_Activity_Code";
        private const string XML_COSTCODE = "PA_Cost_Code";        

        #endregion

        /***********************************************
         * USER/SE: PLEASE DO NOT EDIT BELOW THIS LINE *
         ***********************************************/

        #region Private Globals
        // Active workflow document
        private Document _currentDocument;
        // Application variable
        private Application _app;
        // XML Document
        private XDocument xmlDoc;
        // Document ID/Handle
        private string strDocID;

        //	'Keyword collection for the current document
        private string sCompany = string.Empty;
        private string sDivision = string.Empty;
        private string sInvoiceNum = string.Empty;
        private string sPONum = string.Empty;
        private string sInvoiceDate = string.Empty;
        private string sInvoiceDueDate = string.Empty;
        private string sDiscountDueDate = string.Empty;
        private string sDiscount = string.Empty;
        private string sVendorNum = string.Empty;
        private string sRemitAddrCode = string.Empty;
        private string sTax = string.Empty;
        private string sAdjInvAmt = string.Empty;
        private string sBusDate = string.Empty;
        private string sInvDesc = string.Empty;
        private string sPayMsg = string.Empty;

        private string output = string.Empty;
        #endregion

        #region IWorkflowScript
        /// <summary>
        /// Implementation of <see cref="IWorkflowScript.OnWorkflowScriptExecute" />.
        /// <seealso cref="IWorkflowScript" />
        /// </summary>
        /// <param name="app">Unity Application Object</param>
        /// <param name="args">Workflow Event Arguments</param>
        public void OnWorkflowScriptExecute(Application app, WorkflowEventArgs args)
        {
            try
            {
                // Initialize global settings
                InitializeScript(ref app, ref args);

                // Iterate through keyword records for keyword values
                IterateThroughKeys();

                app.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Verbose, output);

                // Create XML Document
                CreateXMLDoc();

                // Check the current document type and verify if it is an E-Form
                if (_currentDocument.DefaultFileType.Equals("HTML"))
                    EFormXML();

                // Save the XML document to the output path 
                SaveOutDocument();

                args.ScriptResult = true;
            }
            catch (Exception ex)
            {
                // Handle exceptions and log to Diagnostics Console and document history
                HandleException(ex, ref app, ref args);
            }
            finally
            {
                // Log script execution end
                app.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Info,
                string.Format("End Script - [{0}]", ScriptName));
            }
        }
        #endregion

        #region Helper Functions

        /// <summary>
        /// Initialize global settings
        /// </summary>
        /// <param name="app">Unity Application object</param>
        /// <param name="args">Workflow event arguments</param>
        private void InitializeScript(ref Application app, ref WorkflowEventArgs args)
        {
            // Set the specified diagnostics level
            app.Diagnostics.Level = DiagLevel;

            // Log script execution start
            app.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Info,
                string.Format("{0} - Start Script - [{1}]", DateTime.Now.ToString(DateTimeFormat), ScriptName));

            // Capture active document as global
            //_currentDocument = app.Core.GetDocumentByID(TestDocId);
            _currentDocument = args.Document;
            strDocID = _currentDocument.ID.ToString();

            // If an error was stored in the property bag from a previous execution, clear it
            if (args.SessionPropertyBag.ContainsKey(ErrorMessageProperty)) args.SessionPropertyBag.Remove(ErrorMessageProperty);
        }

        /// <summary>
        /// Handle exceptions and log to Diagnostics Console and document history
        /// </summary>
        /// <param name="ex">Exception</param>
        /// <param name="app">Unity Application object</param>
        /// <param name="args">Workflow event arguments</param>
        /// <param name="otherDocument">Document on which to update history if not active workflow document</param>
        private void HandleException(Exception ex, ref Application app, ref WorkflowEventArgs args, Document otherDocument = null)
        {
            var history = app.Core.LogManagement;
            bool isInner = false;

            // Cycle through all inner exceptions
            while (ex != null)
            {
                // Construct error text to store to workflow property
                string propertyError = string.Format("{0}{1}: {2}",
                    isInner ? "Inner " : "",
                    ex.GetType().Name,
                    ex.Message);

                // Construct error text to store to document history
                string historyError = propertyError.Replace(ex.GetType().Name, "Unity Script Error");

                // Construct error text to log to diagnostics console
                string diagnosticsError = string.Format("{0} - ***ERROR***{1}{2}{1}{1}Stack Trace:{1}{3}",
                    DateTime.Now.ToString(DateTimeFormat),
                    Environment.NewLine,
                    propertyError,
                    ex.StackTrace);

                // Add error message to document history (on current or specified document)
                var document = otherDocument ?? _currentDocument;
                if (historyError.Length > 200) historyError = historyError.Substring(0, 199);
                if (document != null && WriteErrorsToHistory) history.CreateDocumentHistoryItem(document, historyError);

                // Write error message to Diagnostcs Consonle
                app.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error, diagnosticsError);

                // Store the original (inner) exception message to error workflow property
                if (ex.InnerException == null) args.SessionPropertyBag.Set(ErrorMessageProperty, ex.Message);

                // Move on to next inner exception
                ex = ex.InnerException;
                isInner = true;
            }

            // Set ScriptResult = false for workflow rules
            args.ScriptResult = false;
        }

        private void EFormXML()
        {
            FieldList eFormFields = _currentDocument.EForm.Fields;

            XDocument eXmlDoc = new XDocument();

            eXmlDoc.Add(new XElement(eFormFields.Find(EF_XMLFIELD).Value));

            IEnumerable<XElement> eNodes = eXmlDoc.Elements(EF_GLLINE);
            if (eNodes == null)
                _app.Diagnostics.Write(string.Format("Nothing found in EForm Field [{0}]", EF_GLLINE));

            foreach (var node in eNodes)
            {
                xmlDoc.Add(new XElement(XML_GLLINE,
                    new XElement(XML_GLCODE, node.Attribute(EF_GLCODE)),
                    new XElement(XML_GLAMT, node.Attribute(EF_GLAMT)),
                    new XElement(XML_PROJNUM, node.Attribute(EF_PROJNUM)),
                    new XElement(XML_ACTCODE, node.Attribute(EF_ACTCODE)),
                    new XElement(XML_COSTCODE, node.Attribute(EF_COSTCODE))
                    ));
            }
        }

        private void IterateThroughKeys()
        {
            // Iterate through all Keyword Records
            foreach (KeywordRecord keywordRecord in _currentDocument.KeywordRecords)
            {
                foreach (Keyword keyword in keywordRecord.Keywords)
                {
                    if (keyword.IsBlank)
                    {
                        output += string.Format("  {0} is blank",
                        keyword.KeywordType.Name);
                    }
                    else
                    {
                        output += string.Format("  {0} = {1}",
                        keyword.KeywordType.Name, keyword.Value.ToString());

                        switch (keyword.KeywordType.Name)
                        {
                            case KW_COMPANY:
                                sCompany = keyword.Value.ToString();
                                break;
                            case KW_DIVISION:
                                sDivision = keyword.Value.ToString();
                                break;
                            case KW_INVOICENUM:
                                sInvoiceNum = keyword.Value.ToString();
                                break;
                            case KW_PONUM:
                                sPONum = keyword.Value.ToString();
                                break;
                            case KW_INVOICEDATE:
                                sInvoiceDate = keyword.Value.ToString();
                                break;
                            case KW_INVOICEDUEDATE:
                                sInvoiceDueDate = keyword.Value.ToString();
                                break;
                            case KW_DISCOUNTDUEDATE:
                                sDiscountDueDate = keyword.Value.ToString();
                                break;
                            case KW_DISCOUNT:
                                sDiscount = keyword.Value.ToString();
                                break;
                            case KW_VENDORNUM:
                                sVendorNum = keyword.Value.ToString();
                                break;
                            case KW_REMITADDRCODE:
                                sRemitAddrCode = keyword.Value.ToString();
                                break;
                            case KW_TAX:
                                sTax = keyword.Value.ToString();
                                break;
                            case KW_ADJINVAMT:
                                sAdjInvAmt = keyword.Value.ToString();
                                break;
                            case KW_BUSDATE:
                                sBusDate = keyword.Value.ToString();
                                break;
                            case KW_INVDESC:
                                sInvDesc = keyword.Value.ToString();
                                break;
                            case KW_PAYMSG:
                                sPayMsg = keyword.Value.ToString();
                                break;
                        }
                    }
                }
            }

            if (string.IsNullOrWhiteSpace(sDiscount))
                sDiscount = ReplaceCharInString(sDiscount);

            if (string.IsNullOrWhiteSpace(sTax))
                sTax = ReplaceCharInString(sTax);

            if (string.IsNullOrWhiteSpace(sAdjInvAmt))
                sAdjInvAmt = ReplaceCharInString(sAdjInvAmt);
        }

        private static string ReplaceCharInString(string replace)
        {
            replace = replace.Replace("$", "");
            replace = replace.Replace(",", "");
            return replace;
        }

        private void CreateXMLDoc()
        {
            xmlDoc = new XDocument(
                    new XElement(XML_ROOT,
                    new XElement(XML_COMPANY, sCompany),
                    new XElement(XML_DIVISION, sDivision),
                    new XElement(XML_INVOICENUM, sInvoiceNum),
                    new XElement(XML_PONUM, sPONum),
                    new XElement(XML_INVOICEDATE, sInvoiceDate),
                    new XElement(XML_INVOICEDUEDATE, sInvoiceDueDate),
                    new XElement(XML_DISCOUNTDUEDATE, sDiscountDueDate),
                    new XElement(XML_DISCOUNT, sDiscount),
                    new XElement(XML_VENDORNUM, sVendorNum),
                    new XElement(XML_REMITADDRCODE, sRemitAddrCode),
                    new XElement(XML_TAX, sTax),
                    new XElement(XML_ADJINVAMT, sAdjInvAmt),
                    new XElement(XML_BUSDATE, sBusDate),
                    new XElement(XML_INVDESC, sInvDesc),
                    new XElement(XML_PAYMSG, sPayMsg)));
        }

        private void SaveOutDocument()
        {
            if (!System.IO.Directory.Exists(DESTPATH))
                System.IO.Directory.CreateDirectory(DESTPATH);

            if (!System.IO.Directory.Exists(DESTPATH))
                throw new ApplicationException(string.Format("Could not create output folder: {0}.", DESTPATH));

            string sFilePath = string.Format(@"{0}\{1}.xml", DESTPATH, strDocID);
            XmlWriterSettings xws = new XmlWriterSettings { OmitXmlDeclaration = true };

            using (XmlWriter xw = XmlWriter.Create(sFilePath, xws))
                xmlDoc.Save(xw);
        }

        #endregion
    }
}
