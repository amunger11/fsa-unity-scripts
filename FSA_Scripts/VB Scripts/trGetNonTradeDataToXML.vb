' trGetNonTradeDataToXML.vbs
' copyright OSAM Document Solutions, Inc.
' written by: Tim Richard
' created on: 06/04/2009
' modified on: 

'Written for FSA

'OnBase Scope: Workflow
'API Type: Automation

'This script gets keywords and e-form fields, parses them, and creates an XML file
'for Infinium

' Used to force variable declaration before usage
Option Explicit

' Site Specific Constants**************************************************

'Export File Path
Const DESTPATH = "C:\test Files\FSANonTrade"

'Keyword Type Names
Const KW_COMPANY = "Company"
Const KW_DIVISION = "Division"
Const KW_INVOICENUM = "Invoice Number"
Const KW_PONUM = "PO Number"
Const KW_INVOICEDATE = "Invoice Date"
Const KW_INVOICEDUEDATE = "Invoice Due Date"
Const KW_DISCOUNTDUEDATE = "Discount Due Date"
Const KW_DISCOUNT = "Discount Amount"
Const KW_VENDORNUM = "Payables Vendor Number"
Const KW_REMITADDRCODE = "Remit to Address Code"
Const KW_TAX = "Adjusted Tax"
Const KW_ADJINVAMT = "Adjusted Invoice Amount"
Const KW_BUSDATE = "Business Date"
Const KW_INVDESC = "Invoice Description"
Const KW_PAYMSG = "Payment Message"

'E-Form Field Names
Const EF_XMLFIELD = "inpGL_ItemsXML"

'E-Form Field Structure
Const EF_GLLINE = "GL_Line"
Const EF_GLCODE = "glCode"
Const EF_GLAMT = "amount"
Const EF_PROJNUM = "projectNum"
Const EF_ACTCODE = "activityCode"
Const EF_COSTCODE = "costCode"

'XML Field Names
Const XML_ROOT = "Non-Trade_Invoice"
Const XML_COMPANY = "Company"
Const XML_DIVISION = "Division"
Const XML_INVOICENUM = "Invoice_Number"
Const XML_PONUM = "PO_Number"
Const XML_INVOICEDATE = "Invoice_Date"
Const XML_INVOICEDUEDATE = "Invoice_Due_Date"
Const XML_DISCOUNTDUEDATE = "Discount_Due_Date"
Const XML_DISCOUNT = "Discount_Amount"
Const XML_VENDORNUM = "Payables_Vendor_Number"
Const XML_REMITADDRCODE = "Remit_to_Address_Code"
Const XML_TAX = "Adjusted_Tax"
Const XML_ADJINVAMT = "Invoice_Amount"
Const XML_BUSDATE = "Business_Date"
Const XML_INVDESC = "Invoice_Description"
Const XML_PAYMSG = "Payment_Message"
Const XML_GLLINE = "GL_Line"
Const XML_GLCODE = "GL_Non-Trade_Account_Code"
Const XML_GLAMT = "GL_Non-Trade_Item_Amount"
Const XML_PROJNUM = "PA_Project_Number"
Const XML_ACTCODE = "PA_Activity_Code"
Const XML_COSTCODE = "PA_Cost_Code"

' Site Specific Constants**************************************************

' Preferred method to write VB scripts with OnBase
Sub Main35()
	' Root level OnBase Automation object
	Dim oApp: Set oApp = CreateObject("OnBase.Application")	
	
	' Currently active document returned by the root automation object
	' The script will only have one CurrentDocument during each execution	
	Dim oDoc: Set oDoc = oApp.CurrentDocument
	
	'Get doc handle, for naming the file
	Dim lDocID: lDocID = oDoc.Handle

	' PropertyBag object used to carry over data across script executions	
	Dim oPropBag: Set oPropBag = oApp.PropertyBag
	
	'Keyword collection for the current document
	Dim cKW: Set cKW = oDoc.Keywords
	
	'dimension vars for xml output, assign "" by default
	Dim sCompany: sCompany = ""
	Dim sDivision: sDivision = ""
	Dim sInvoiceNum: sInvoiceNum = ""
	Dim sPONum: sPONum = ""
	Dim sInvoiceDate: sInvoiceDate = ""
	Dim sInvoiceDueDate: sInvoiceDueDate = ""
	Dim sDiscountDueDate: sDiscountDueDate = ""
	DIm sDiscount: sDiscount = ""
	Dim sVendorNum: sVendorNum = ""
	Dim sRemitAddrCode: sRemitAddrCode = ""
	Dim sTax: sTax = ""
	Dim sAdjInvAmt: sAdjInvAmt = ""
	Dim sBusDate: sBusDate = ""
	Dim sInvDesc: sInvDesc = ""
	Dim sPayMsg: sPayMsg = ""
	
	
	'Loop through keywords, creating nodes as we go
	Dim i: i=0
	For i=0 to cKW.Count - 1
	    Select Case cKW.Item(i).Name
	        Case KW_COMPANY
                sCompany = cKW.Item(i).Value
	        Case KW_DIVISION
                sDivision = cKW.Item(i).Value
	        Case KW_INVOICENUM
                sInvoiceNum = cKW.Item(i).Value
	        Case KW_PONUM
                sPONum = cKW.Item(i).Value
	        Case KW_INVOICEDATE
                sInvoiceDate = cKW.Item(i).Value
	        Case KW_INVOICEDUEDATE
                sInvoiceDueDate = cKW.Item(i).Value
	        Case KW_DISCOUNTDUEDATE
                sDiscountDueDate = cKW.Item(i).Value
	        Case KW_DISCOUNT
	            sDiscount = StripCurr(cKW.Item(i).Value)	            
	        Case KW_VENDORNUM
                sVendorNum = cKW.Item(i).Value
	        Case KW_REMITADDRCODE
                sRemitAddrCode = cKW.Item(i).Value
	        Case KW_TAX
	            sTax = StripCurr(cKW.Item(i).Value)
	        Case KW_ADJINVAMT
	            sAdjInvAmt = StripCurr(cKW.Item(i).Value)
	        Case KW_BUSDATE
                sBusDate = cKW.Item(i).Value
	        Case KW_INVDESC
                sInvDesc = cKW.Item(i).Value
	        Case KW_PAYMSG
                sPayMsg = cKW.Item(i).Value
	    End Select	
	Next	
	
	'xmlDocument (in memory)
	'Create the XMLDOM object
	Dim xmlDoc: Set xmlDoc = CreateObject("MSXML.DOMDocument")
	
	'create base node
	Dim xmlRoot: Set xmlRoot = xmlDoc.createElement(XML_ROOT)
	'create node variable
	Dim xmlNode
	'create text node (value for node)
	Dim xmlText
	
	'Appending XML data consists of the following calls:
	'create new node for information element
	'Set xmlNode = xmlDoc.createElement(<XML_NODENAME>)
	'create text node to hold text data for element
    'Set xmlText = xmlDoc.createTextNode(cKW.Item(i).Value)
    'apply textnode (text data) to element
    'Call xmlNode.appendChild(xmlText)
    'append element to root node
    'Call xmlRoot.appendChild(xmlNode)
    'eventually, we append the rootnode to the xmldocument
    'Call xmlDoc.appendChild(xmlRoot)
        
	
    Set xmlNode = xmlDoc.createElement(XML_COMPANY)
    Set xmlText = xmlDoc.createTextNode(sCompany)
    Call xmlNode.appendChild(xmlText)
    Call xmlRoot.appendChild(xmlNode)

    Set xmlNode = xmlDoc.createElement(XML_DIVISION)
    Set xmlText = xmlDoc.createTextNode(sDivision)
    Call xmlNode.appendChild(xmlText)
    Call xmlRoot.appendChild(xmlNode)

    Set xmlNode = xmlDoc.createElement(XML_INVOICENUM)
    Set xmlText = xmlDoc.createTextNode(sInvoiceNum)
    Call xmlNode.appendChild(xmlText)
    Call xmlRoot.appendChild(xmlNode)

    Set xmlNode = xmlDoc.createElement(XML_PONUM)
    Set xmlText = xmlDoc.createTextNode(sPONum)
    Call xmlNode.appendChild(xmlText)
    Call xmlRoot.appendChild(xmlNode)

    Set xmlNode = xmlDoc.createElement(XML_INVOICEDATE)
    Set xmlText = xmlDoc.createTextNode(sInvoiceDate)
    Call xmlNode.appendChild(xmlText)
    Call xmlRoot.appendChild(xmlNode)

    Set xmlNode = xmlDoc.createElement(XML_INVOICEDUEDATE)
    Set xmlText = xmlDoc.createTextNode(sInvoiceDueDate)
    Call xmlNode.appendChild(xmlText)
    Call xmlRoot.appendChild(xmlNode)

    Set xmlNode = xmlDoc.createElement(XML_DISCOUNTDUEDATE)
    Set xmlText = xmlDoc.createTextNode(sDiscountDueDate)
    Call xmlNode.appendChild(xmlText)
    Call xmlRoot.appendChild(xmlNode)

    Set xmlNode = xmlDoc.createElement(XML_DISCOUNT)
    Set xmlText = xmlDoc.createTextNode(sDiscount)
    Call xmlNode.appendChild(xmlText)
    Call xmlRoot.appendChild(xmlNode)

    Set xmlNode = xmlDoc.createElement(XML_VENDORNUM)
    Set xmlText = xmlDoc.createTextNode(sVendorNum)
    Call xmlNode.appendChild(xmlText)
    Call xmlRoot.appendChild(xmlNode)

    Set xmlNode = xmlDoc.createElement(XML_REMITADDRCODE)
    Set xmlText = xmlDoc.createTextNode(sRemitAddrCode)
    Call xmlNode.appendChild(xmlText)
    Call xmlRoot.appendChild(xmlNode)

    Set xmlNode = xmlDoc.createElement(XML_TAX)
    Set xmlText = xmlDoc.createTextNode(sTax)
    Call xmlNode.appendChild(xmlText)
    Call xmlRoot.appendChild(xmlNode)

    Set xmlNode = xmlDoc.createElement(XML_ADJINVAMT)
    Set xmlText = xmlDoc.createTextNode(sAdjInvAmt)
    Call xmlNode.appendChild(xmlText)
    Call xmlRoot.appendChild(xmlNode)

    Set xmlNode = xmlDoc.createElement(XML_BUSDATE)
    Set xmlText = xmlDoc.createTextNode(sBusDate)
    Call xmlNode.appendChild(xmlText)
    Call xmlRoot.appendChild(xmlNode)
    
    Set xmlNode = xmlDoc.createElement(XML_INVDESC)
    Set xmlText = xmlDoc.createTextNode(sInvDesc)
    Call xmlNode.appendChild(xmlText)
    Call xmlRoot.appendChild(xmlNode)

    Set xmlNode = xmlDoc.createElement(XML_PAYMSG)
    Set xmlText = xmlDoc.createTextNode(sPayMsg)
    Call xmlNode.appendChild(xmlText)
    Call xmlRoot.appendChild(xmlNode)
	
	Call xmlDoc.appendChild(xmlRoot)
		
	'Field collection for the current document (this only applies to e-forms)
	Dim cFields: Set cFields = oDoc.Fields
	
	'Value of the xmlField to parse
	Dim eXMLDoc: Set eXMLDoc = CreateObject("MSXML.DOMDocument")

	'Iterate through the document fields collection and set xmlField
	For i = 0 to cFields.Count - 1
		If cFields.Item(i).Name = EF_XMLFIELD Then
			Call eXMLDoc.loadXML(cFields.Item(i).Value)
			Exit For
		End If	
	Next
	
	'Get Variance_Lines
	Dim eNodes: Set eNodes = eXMLDoc.getElementsByTagName(EF_GLLINE)
	'create parentNode var
	Dim xmlParent
	For i = 0 to eNodes.length - 1
	    'Create parent Node for GL_Line  
        Set xmlParent = xmlDoc.createElement(XML_GLLINE)
        'create glCode element
        Set xmlNode = xmlDoc.createElement(XML_GLCODE)
        'set text value
        Set xmlText = xmlDoc.createTextNode(eNodes(i).getAttribute(EF_GLCODE))
        'append text
        Call xmlNode.appendChild(xmlText)
        'append child to Parent
        Call xmlParent.appendChild(xmlNode)
        
        'create glAmount element
        Set xmlNode = xmlDoc.createElement(XML_GLAMT)
        Set xmlText = xmlDoc.createTextNode(StripCurr(eNodes(i).getAttribute(EF_GLAMT)))
        Call xmlNode.appendChild(xmlText)
        Call xmlParent.appendChild(xmlNode)
        
        'create projNum element
        Set xmlNode = xmlDoc.createElement(XML_PROJNUM)
        Set xmlText = xmlDoc.createTextNode(eNodes(i).getAttribute(EF_PROJNUM))
        Call xmlNode.appendChild(xmlText)
        Call xmlParent.appendChild(xmlNode)
        
        'create actCode element
        Set xmlNode = xmlDoc.createElement(XML_ACTCODE)
        Set xmlText = xmlDoc.createTextNode(eNodes(i).getAttribute(EF_ACTCODE))
        Call xmlNode.appendChild(xmlText)
        Call xmlParent.appendChild(xmlNode)
        
        'create costCode element
        Set xmlNode = xmlDoc.createElement(XML_COSTCODE)
        Set xmlText = xmlDoc.createTextNode(eNodes(i).getAttribute(EF_COSTCODE))
        Call xmlNode.appendChild(xmlText)
        Call xmlParent.appendChild(xmlNode)        

        'append parent to root
        Call xmlRoot.appendChild(xmlParent)
	Next
	
	Dim oFSO: Set oFSO = CreateObject("Scripting.FileSystemObject")
	'create folder if it doesn't exist
	If Not (oFSO.FolderExists(DESTPATH)) Then
		Call oFSO.CreateFolder(DESTPATH)
	End If
	
    Dim sFilePath: sFilePath =  DESTPATH & "\" & cStr(lDocID) & ".xml"
    Dim oFile: Set oFile = oFSO.CreateTextFile(sFilePath, TRUE)
    Call oFile.Write(xmlDoc.xml)
    Call oFile.Close()
	
End Sub 'Main35

'Parse

'strips commas and $ from currency values
Function StripCurr(val)
    val = Replace(val,"$","")
    val = Replace(val,",","")
    
    StripCurr=val
End Function'StripCurr